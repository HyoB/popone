package com.hyob.popone.utils

import android.net.Uri
import android.util.Log

class PoponeLog {

    companion object {

        fun error(cls: Class<*>, method: String, message: String?) {
            Log.e("[${cls.name} : $method]", message)
        }

        fun debug(cls: Class<*>, method: String, message: String?) {
            Log.d("[${cls.name} : $method]", message)
        }

    }

}