package com.hyob.popone

import android.app.Application
import com.hyob.popone.retrofit.api.KakaoRestApi
import com.hyob.popone.retrofit.api.KakaoRestApiModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by dev_hyob on 2018-06-26.
 */
@Singleton
@Component(modules = arrayOf(PoponeModule::class, KakaoRestApiModule::class))
interface PoponeComponent {

    fun getApplication(): Application

    fun getKakaoRestApi(): KakaoRestApi

}