package com.hyob.popone.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.hyob.popone.R
import com.hyob.popone.model.KakaoImageVo
import com.hyob.popone.view.PagingRecyclerView
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.holder_kakao_image.view.*

/**
 * Created by dev_hyob on 2018-06-22.
 */
class MainAdapter: RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    private val items by lazy { ArrayList<KakaoImageVo>() }
    val publishSubject by lazy { PublishSubject.create<KakaoImageVo>() }

    fun update(items: List<KakaoImageVo>?) {
        with(this.items) {
            addAll(checkNotNull(items))
            notifyDataSetChanged()
        }
    }

    fun clear(){
        items.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.holder_kakao_image, parent, false)
        return MainViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder?.bindView(items.get(position))
    }

    inner class MainViewHolder(itemView: View?): RecyclerView.ViewHolder(itemView) {

        fun bindView(item: KakaoImageVo) = with(itemView) {
            val option = RequestOptions()
            option.centerCrop()
            Glide.with(itemView).load(item.thumbnail).apply(option).into(image_thumbnail)
            setOnClickListener {
                publishSubject.onNext(item)
            }
        }
    }
}