package com.hyob.popone.view.base

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.Disposable

/**
 * Created by dev_hyob on 2018-06-21.
 */
interface BaseContract {

    interface View {

        fun initialize(savedInstanceState: Bundle?)

        fun context(): Context

        fun error(e: Exception)

        fun keyboardHide()

        fun show(root: ViewGroup, display: android.view.View)

        fun toast(message: String)

    }

    interface Presenter<V: View> {

        fun onAttach(view: V)

        fun onDetach()

        fun execute(disposable: Disposable)

        fun execute(vararg disposable: Disposable)

        fun onFinish()

        fun onError(e: Exception)

    }

}