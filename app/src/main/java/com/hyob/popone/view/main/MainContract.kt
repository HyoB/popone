package com.hyob.popone.view.main

import com.hyob.popone.model.KakaoImageListVo
import com.hyob.popone.model.KakaoImageVo
import com.hyob.popone.view.base.BaseContract
import kotlin.collections.ArrayList

/**
 * Created by dev_hyob on 2018-06-21.
 */

interface MainContract {

    interface View: BaseContract.View {

        fun searchButtonClicked()

        fun updateKakaoImages(imageList: KakaoImageListVo?)

        fun showImageDetailView(kakaoImageVo: KakaoImageVo?)

    }

    interface Presenter<V: View>: BaseContract.Presenter<V> {

        fun requestKakaoApi(query: String, page: Int, size: Int)

    }

}