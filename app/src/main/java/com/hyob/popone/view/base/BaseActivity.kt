package com.hyob.popone.view.base

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.design.widget.CoordinatorLayout
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.Toast
import com.hyob.popone.PoponeApplication
import com.hyob.popone.PoponeComponent

/**
 * Created by dev_hyob on 2018-06-21.
 */
abstract class BaseActivity : AppCompatActivity(), BaseContract.View {

    val inputMethodManager by lazy { (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager) }

    override fun context(): Context {
        return this
    }

    override fun keyboardHide() {
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

    override fun show(root: ViewGroup, display: View) {
        display.layoutParams = root.layoutParams
        root.addView(display)
    }

    override fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun error(e: Exception) {
        Log.e("popone error", e.message)
    }

}