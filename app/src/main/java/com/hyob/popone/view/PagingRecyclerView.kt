package com.hyob.popone.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.nfc.tech.MifareUltralight.PAGE_SIZE
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.hyob.popone.utils.PoponeLog
import io.reactivex.subjects.PublishSubject


class PagingRecyclerView : RecyclerView {

    constructor(context: Context?): super(context)
    constructor(context: Context?, attrs: AttributeSet?): super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int): super(context, attrs, defStyle)

    val pageingSubject = PublishSubject.create<Int>()
    var loading: Boolean = false
    var currentPage: Int = 1
    val PAGE_SIZE = 30

    init {
        addScrollListener()
    }

    fun addScrollListener() {
        addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                (layoutManager as LinearLayoutManager)?.run {
                    if(!loading) {
                        val firstVisiblePosition = findFirstVisibleItemPosition()
                        val curnentPosition = childCount + firstVisiblePosition
                        val totalCount = itemCount

                        if (curnentPosition >= totalCount && firstVisiblePosition >= 0 && totalCount >= PAGE_SIZE) {
                            loading = true
                            currentPage += 1
                            pageingSubject.onNext(currentPage)
                        }
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }

}