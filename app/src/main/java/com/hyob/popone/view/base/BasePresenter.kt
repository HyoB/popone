package com.hyob.popone.view.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by dev_hyob on 2018-06-21.
 */
open class BasePresenter<V: BaseContract.View> : BaseContract.Presenter<V> {

    private var baseView: V? = null
    val compositeDisposable by lazy { CompositeDisposable() }

    override fun onAttach(view: V) {
        baseView = view
    }

    override fun onDetach() {
        baseView = null
    }

    override fun execute(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun execute(vararg disposable: Disposable) {
        compositeDisposable.addAll(*disposable)
    }

    override fun onFinish() {
        baseView = null
        compositeDisposable.clear()
    }

    override fun onError(e: Exception) {
        baseView?.error(e)
    }

    fun getView(): V? {
        return baseView
    }
}