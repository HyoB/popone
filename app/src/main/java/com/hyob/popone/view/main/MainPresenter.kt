package com.hyob.popone.view.main

import com.hyob.popone.PoponeApplication
import com.hyob.popone.R
import com.hyob.popone.exception.PoponeException
import com.hyob.popone.view.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by dev_hyob on 2018-06-21.
 */

class MainPresenter<V: MainContract.View> : BasePresenter<V>(), MainContract.Presenter<V> {

    override fun requestKakaoApi(query: String, page: Int, size: Int) {
        getView()?.context()?.getString(R.string.header_kakao).let { header ->
            try {
                PoponeApplication.get(getView()?.context()!!)
                        .getPoponeComponent()
                        .getKakaoRestApi()
                        .requestImagesFromKakao(header!!, query, null, page, size)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnError {
                            onError(PoponeException.NetworkFail(it.message))
                        }
                        .subscribe {
                            getView()?.updateKakaoImages(it)
                        }
            } catch (npe: NullPointerException) {
                onError(PoponeException.TemporaryException(npe.message))
            }
        }
    }

}