package com.hyob.popone.view.image

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.bumptech.glide.Glide
import com.hyob.popone.R
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_image_detail.view.*

/**
 * Created by dev_hyob on 2018-06-22.
 *
 * Only use it programatically
 */
class ImageDetailView(context: Context?) : FrameLayout(context) {

    val closeObservable = PublishSubject.create<View>()
    var url: String? = null
        set(value) {
            field = value;
            Glide.with(this).load(url).into(image_detail)
        }

    init {
        initialize()
    }

    fun initialize(){
        LayoutInflater.from(context).inflate(R.layout.activity_image_detail, this)
        Glide.with(this).load(url).into(image_detail)
        button_close.setOnClickListener {
            closeObservable.onNext(this@ImageDetailView)
        }
    }
}