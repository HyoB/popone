package com.hyob.popone.view.detail

import android.os.Bundle
import com.bumptech.glide.Glide
import com.hyob.popone.R
import com.hyob.popone.model.KakaoImageVo
import com.hyob.popone.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_image_detail.*

class DetailImageActivity: BaseActivity() {

    companion object {
        val KEY_IMAGE_URL = DetailImageActivity::class.java.name + "image"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_detail)
        initialize(savedInstanceState)
    }

    override fun initialize(savedInstanceState: Bundle?) {
        intent.extras.getParcelable<KakaoImageVo>(KEY_IMAGE_URL).let { it ->
            Glide.with(this).load(it.image).into(image_detail)
        }
        button_close.setOnClickListener {
            finish()
        }
    }
}