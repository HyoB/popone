package com.hyob.popone.view.main

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.hyob.popone.R
import com.hyob.popone.exception.PoponeException
import com.hyob.popone.model.KakaoImageListVo
import com.hyob.popone.model.KakaoImageVo
import com.hyob.popone.utils.PoponeLog
import com.hyob.popone.view.adapter.MainAdapter
import com.hyob.popone.view.base.BaseActivity
import com.hyob.popone.view.detail.DetailImageActivity
import com.hyob.popone.view.image.ImageDetailView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_search.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainContract.View, TextView.OnEditorActionListener {

    val presenter by lazy { MainPresenter<MainContract.View>() }

    private val mainAdapter by lazy { MainAdapter() }
    private val RECYCLER_PAGE_SIZE by lazy { recycler_images.PAGE_SIZE }
    private val imageDetailView by lazy { ImageDetailView(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onAttach(this)
        initialize(savedInstanceState)
    }

    override fun initialize(savedInstanceState: Bundle?) {
        recycler_images?.run {
            adapter = mainAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
        button_search?.run {
            setOnClickListener {
                searchButtonClicked()
            }
        }
        edit_search.setOnEditorActionListener(this)
        presenter.execute(
                mainAdapter.publishSubject
                        .doOnError {
                            error(PoponeException.ClickException(it.message))
                        }
                        .subscribe {
                            showImageDetailView(it)
                        },
                recycler_images.pageingSubject
                        .doOnError {
                            error(PoponeException.ClickException(it.message))
                        }
                        .subscribe { page ->
                            presenter.requestKakaoApi(edit_search.text.toString(), page, RECYCLER_PAGE_SIZE)
                        },
                imageDetailView.closeObservable
                        .doOnError {
                            error(PoponeException.ClickException(it.message))
                        }
                        .subscribe {
                            layout_root.removeView(it)
                        })

    }

    override fun searchButtonClicked() {
        mainAdapter.clear()
        recycler_images.currentPage = 1
        presenter.requestKakaoApi(edit_search.text.toString(), 1, RECYCLER_PAGE_SIZE)
        keyboardHide()
    }

    override fun updateKakaoImages(imageList: KakaoImageListVo?) {
        imageList?.images?.run {
            mainAdapter.update(this)
            recycler_images.loading = false
        }
    }

    override fun showImageDetailView(kakaoImageVo: KakaoImageVo?) {
        Intent(this, DetailImageActivity::class.java).let {
            it.putExtra(DetailImageActivity.KEY_IMAGE_URL, kakaoImageVo)
            startActivity(it)
        }
    }

    override fun error(e: Exception) {
        super.error(e)
        when(e) {
            is PoponeException.NetworkFail -> {
                toast(getString(R.string.error_network))
            }
            is PoponeException.ClickException -> {
                toast(getString(R.string.error_temporary))
            }
            is PoponeException.TemporaryException -> {
                toast(getString(R.string.error_temporary))
            }
        }
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        when(actionId) {
            EditorInfo.IME_ACTION_SEARCH -> {
                searchButtonClicked()
                return true
            }
        }
        return false
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onFinish()
    }
}
