package com.hyob.popone

import android.app.Application
import android.content.Context
import com.hyob.popone.retrofit.api.KakaoRestApiModule

/**
 * Created by dev_hyob on 2018-06-26.
 */
class PoponeApplication: Application() {

    private val component by lazy {
        DaggerPoponeComponent.builder()
                .poponeModule(PoponeModule(this))
                .kakaoRestApiModule(KakaoRestApiModule())
                .build()
    }

    companion object {

        fun get(context: Context): PoponeApplication {
            return context.applicationContext as PoponeApplication
        }

    }

    fun getPoponeComponent(): PoponeComponent {
        return component
    }

}