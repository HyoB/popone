package com.hyob.popone.exception

/**
 * Created by dev_hyob on 2018-06-22.
 */
class PoponeException {

    class NetworkFail(override val message: String?): Exception(message)

    class ClickException(override val message: String?): Exception(message)

    class TemporaryException(override val message: String?): Exception(message)

}