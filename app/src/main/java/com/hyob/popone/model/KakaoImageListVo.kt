package com.hyob.popone.model

import com.google.gson.annotations.SerializedName

/**
 * Created by dev_hyob on 2018-06-22.
 */
data class KakaoImageListVo(@SerializedName("documents") val images: ArrayList<KakaoImageVo>?)