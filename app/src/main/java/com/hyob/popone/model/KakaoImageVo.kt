package com.hyob.popone.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by dev_hyob on 2018-06-21.
 */
data class KakaoImageVo(
        @SerializedName("thumbnail_url") val thumbnail: String?,
        @SerializedName("image_url") val image: String?) : Parcelable {
    override fun toString(): String {
        return thumbnail ?: "Not Initialized"
    }

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(thumbnail)
        writeString(image)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<KakaoImageVo> = object : Parcelable.Creator<KakaoImageVo> {
            override fun createFromParcel(source: Parcel): KakaoImageVo = KakaoImageVo(source)
            override fun newArray(size: Int): Array<KakaoImageVo?> = arrayOfNulls(size)
        }
    }
}

