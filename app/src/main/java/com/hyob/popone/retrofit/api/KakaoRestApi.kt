package com.hyob.popone.retrofit.api

import com.hyob.popone.model.KakaoImageListVo
import com.hyob.popone.model.KakaoImageVo
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

/**
 * Created by dev_hyob on 2018-06-21.
 */
interface KakaoRestApi {

    @GET("/v2/search/image")
    fun requestImagesFromKakao(
            @Header("Authorization") header: String,
            @Query("query") query: String,
            @Query("sort") sort: String?,
            @Query("page") page: Int?,
            @Query("size") size: Int?): Observable<KakaoImageListVo>

}