package com.hyob.popone.retrofit.api

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by dev_hyob on 2018-06-26.
 */

@Module
class KakaoRestApiModule {

    private val BASE_URL = "https://dapi.kakao.com"

    @Provides
    @Singleton
    fun make() : Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun getKakaoRestApi(retrofit: Retrofit):KakaoRestApi{
        return retrofit.create(KakaoRestApi::class.java)
    }

}