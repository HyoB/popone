package com.hyob.popone

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by dev_hyob on 2018-06-26.
 */
@Module
class PoponeModule(private val application: Application) {

    @Provides
    @Singleton
    fun getApplication(): Application {
        return application
    }
}